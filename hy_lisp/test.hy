(import math)
(print (math.cos math.pi)) ; calling python function

(defclass Triangle [object]
    (setv 
        x 0
        y 0)
    (defn move-right [ self moveby ]
        (setv self.x (+ self.x moveby))))
(setv myt (Triangle ))
(myt.move-right 100)
(print myt.x)
