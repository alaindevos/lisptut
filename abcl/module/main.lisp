(load "~/quicklisp/setup.lisp")
(declaim (optimize (speed 3) (safety 3) (space 0) (debug 3)))

(load "library.abcl")

(in-package :cl)
(defun main()
	(package-library:printstring "Hello World"))
(main)
(cl-user::quit)
