(declaim (optimize (speed 3) (safety 3) (space 0) (debug 3)))
(load "~/quicklisp/setup.lisp")

(format t "~a~%" "COMPILING library.lisp")
(CL:COMPILE-FILE "library.lisp")

(format t "~a~%" "COMPILING main.lisp")
(CL:COMPILE-FILE "main.lisp")

(format t "~a~%" "Done compiling")
(quit)
