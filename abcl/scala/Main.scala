// Scala 2 uses _, Scala 3 uses * like Java
import org.armedbear.lisp.*
// Static stuff goes in objects
object Main {
	//@static // annotation in Scala 3 forces generation of a static method 
	def addTwoNumbers(a: Int, b: Int): Int =
		a + b // Scala automatically returns expressions
}
