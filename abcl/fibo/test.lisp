(declaim (optimize (speed 3) (safety 1) (space 0) (debug 1)))
(load "~/quicklisp/setup.lisp")
(ql:quickload "defstar")
;(defun* (fibo -> integer) ((x integer))
(defun fibo (x)
    (if (< x 2) 
        1
        (+ (fibo (- x 2)) 
           (fibo (- x 1)))))

(defun main ()
    (format t "~a~%" (fibo 39)))

(main)
(cl-user::quit)
;45s
