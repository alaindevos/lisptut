(declaim (optimize (speed 3) (safety 1) (space 0) (debug 1)))
(load "~/quicklisp/setup.lisp")
(ql:quickload "defstar")

(defparameter a (jnew "java.util.ArrayList"))

(defun main ()
    (jcall "add" a 5)
    (jcall "add" a 7)
    (format t "~a" a))
    
(main)
(cl-user::quit)
