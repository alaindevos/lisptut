(declaim (optimize (speed 3) (safety 3) (space 0) (debug 3)))
(load "~/quicklisp/setup.lisp")

(format t "~a~%" "COMPILING test.lisp")
(CL:COMPILE-FILE "test.lisp")

(quit)
