(declaim (optimize (speed 3) (safety 3) (space 0) (debug 3)))

(load "~/quicklisp/setup.lisp")
(ql:quickload "alexandria")

;(ql:quickload "sb-introspect")
;(ql:quickload "sb-cltl2")

(require :abcl-contrib)
(require :abcl-asdf)

(format t " ~a ~% "(asdf:already-loaded-systems))
(asdf:load-systems :cloop)
(format t " ~a ~% "(asdf:already-loaded-systems))
(cloop:printme "Hello World")

(cl-user::quit)
